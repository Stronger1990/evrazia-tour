<?php
/*
Template Name: Страница с контентом
*/
get_header();
?>
<div class="container-content-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="page-content-block-wrapper">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="product-block-wrapper">
                                <?php
                                            do_action('woocommerce_custom_breadcrumb');
                                            ?>
                            </div>
                        </div>
                    </div>
                    <?php
                                while ( have_posts() ) :
                                    the_post();

                                    get_template_part( 'template-parts/content', 'page' );

                                    // If comments are open or we have at least one comment, load up the comment template.
                                    if ( comments_open() || get_comments_number() ) :
                                        comments_template();
                                    endif;

                                endwhile; // End of the loop.
                                ?>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
