<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package evrazia-tour
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(53737535, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/53737535" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<meta name="yandex-verification" content="d48241cd70ec1e24" />
</head>

<body>
    <header>
        <div class="container-desktop-header hidden-xs hidden-sm">
            <div class="container-header">
                <div class="container-fluid">
                    <div class="header-content-block">
                        <div class="wrap-header-content-top">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <div class="header-content-block-logo">
                                        <a href="<?php echo get_home_url(); ?>" class="link-main"><img class="" src="<?php echo get_template_directory_uri() . '/assets/img/logo.png'?>"></a>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-9">
                                    <div class="header-content-block-menu">
                                         <?php wp_nav_menu('menu=Main-menu'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrap-header-content-midle">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="header-content-midle">
                                        <div class="header-content-block-social">
                                            <a href="https://vk.com/id186632369"><img class="" src="<?php echo get_template_directory_uri() . '/assets/img/icons/ico-vk.png'?>"></a>
                                            <a href="https://www.facebook.com/evrazia.yar/"><img class="" src="<?php echo get_template_directory_uri() . '/assets/img/icons/ico-fb.png'?>"></a>
                                            <a href="https://ok.ru/profile/515973582810"><img class="" src="<?php echo get_template_directory_uri() . '/assets/img/icons/ico-ok.png'?>"></a>
                                            <a href="https://www.instagram.com/evrazia_tour/"><img class="" src="<?php echo get_template_directory_uri() . '/assets/img/icons/ico-inst.png'?>"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="header-content-block-phone">
                                        <div class="content-block-phone">
                                            <img class="" src="<?php echo get_template_directory_uri() . '/assets/img/ico-phone.png'?>">
                                            <a href="tel:88002224656">8 800 222 46 56</a>
                                            <button data-toggle="modal" data-target="#myModalcall" class="btn-call">Заказать звонок</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-mobile-header hidden-md hidden-lg">
            <div id="mobile-icon-menu" class="header-icon header-mobile-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 24 24" enable-background="new 0 0 24 24">
                    <g>
                        <path d="M24,3c0-0.6-0.4-1-1-1H1C0.4,2,0,2.4,0,3v2c0,0.6,0.4,1,1,1h22c0.6,0,1-0.4,1-1V3z"></path>
                        <path d="M24,11c0-0.6-0.4-1-1-1H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h22c0.6,0,1-0.4,1-1V11z"></path>
                        <path d="M24,19c0-0.6-0.4-1-1-1H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h22c0.6,0,1-0.4,1-1V19z"></path>
                    </g>
                </svg>
            </div>
            <div class="header-mobile-logo">
                <a href="<?php echo get_home_url(); ?>" class="header-logo-link">
                    <img src="<?php echo get_template_directory_uri() . '/assets/img/logo.png'?>" alt="Евразия Тур">
                </a>
            </div>
            <div class="header-icon header-mobile-icon">
                <a href="tel:8800222465" class="reset-color">
                    <span class="glyphicon glyphicon-earphone"></span>
                </a>
            </div>
            <!--header tablet menu-->
            <div id="header-menu-widget" class="header-menu-widget-container">
                <div class="header-menu-widget">
                    
                    <div class="header-tablet-menu-list">
                        <?php wp_nav_menu('menu=Main-menu'); ?>
                    </div>
                </div>
            </div>
        </div>
    </header><!-- #masthead -->

	<div id="content" class="site-content">
