<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package evrazia-tour
 */

?>
	</div><!-- #content -->

	<footer>
        <div class="container-footer">
            <div class="footer-content-top">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="footer-menu-block">
                                <div class="footer-content-block-list">
                                    <ul class="footer-list">
                                        <li class="footer-inner-item"><a href="http://evrazia-tour.ru/">Главная</a></li>
                                        <li class="footer-inner-item"><a href="http://evrazia-tour.ru/goryashhie-tury/">Горящие туры</a></li>
                                        <li class="footer-inner-item"><a href="http://evrazia-tour.ru/strany/">Страны</a></li>
                                    </ul>
                                </div>
                                <div class="footer-content-block-logo">
                                    <a href="#"><img class="" src="<?php echo get_template_directory_uri() . '/assets/img/logo.png'?>"></a>
                                </div>
                                <div class="footer-content-block-social">
                                    <a href="https://vk.com/id186632369"><img class="" src="<?php echo get_template_directory_uri() . '/assets/img/icons/ico-vk.png'?>"></a>
                                    <a href="https://www.facebook.com/evrazia.yar/"><img class="" src="<?php echo get_template_directory_uri() . '/assets/img/icons/ico-fb.png'?>"></a>
                                    <a href="https://ok.ru/profile/515973582810"><img class="" src="<?php echo get_template_directory_uri() . '/assets/img/icons/ico-ok.png'?>"></a>
                                    <a href="https://www.instagram.com/evrazia_tour/"><img class="" src="<?php echo get_template_directory_uri() . '/assets/img/icons/ico-inst.png'?>"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="footer-menu-block-list">
                                <ul class="footer-list">
                                    <li class="footer-inner-item"><a href="http://evrazia-tour.ru/o-nas/">О нас</a></li>
                                    <li class="footer-inner-item"><a href="http://evrazia-tour.ru/rassrochka/">Рассрочка</a></li>
                                    <li class="footer-inner-item"><a href="http://evrazia-tour.ru/kontakty/">Контакты</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-5">
                            <div class="footer-contact-block">
                                <div class="content-block-phone">
                                    <img class="" src="<?php echo get_template_directory_uri() . '/assets/img/ico-phone.png'?>">
                                    <a href="#">8 800 222 46 56</a>
                                    <button data-toggle="modal" data-target="#myModalcall" class="btn-call">Заказать звонок</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-content-bottom">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                           <div class="visa-icons">
                               <p>Copyright © 2019 Евразия Тур</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                           <div class="visa-icons">
                               <img class="" src="<?php echo get_template_directory_uri() . '/assets/img/icons/visa-icons.png'?>">
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModalcall" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close btn-call-modal" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body-wrap">
                        <?php echo do_shortcode('[contact-form-7 id="53" title="Заказать звонок"]'); ?>
                    </div>
                </div>
            </div>
        </div>
          <!-- Modal -->
    </footer>

<?php wp_footer(); ?>

</body>
</html>
