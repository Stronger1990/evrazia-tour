$(document).ready(function () {

    $(".toggle-btn").click(function () {
        $(".toggle-menu").toggleClass("visible");
    });
    
    $(".list-item").each(function(index, element){
        
       var $item = $(element);
        
        $item.click(function(){
            $(this).find(".lower-sub-menu").toggleClass("visible-block");
            $(this).find(".fa-chevron-circle-right").toggleClass("rotation");
        });
    });
});

