<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'evraziat_wp_evrazia_tour' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'evraziat_wp_samo' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'thoo1Num' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'PEzhe6Uyc;fh/8hJDxjThxP:9r(pGVkF<3ml@u@T6 tTYPPI,-jbDl[F;T&fDSS8' );
define( 'SECURE_AUTH_KEY',  '9LiydaChKaA`.|ZkL6sg5}i{C0)k[lBfVG9hPWUv]V{uk*a0z%5C:h XC*riN+_~' );
define( 'LOGGED_IN_KEY',    '*o_uL4(!&@:unh_*laNsQO+!T%oG;0m2_Y@7.D`M+[Pg#/*XqBG=+VOa(2JW 80a' );
define( 'NONCE_KEY',        'd,_)!&%Ux9^A(y>S]h(>7[{FWz9aa$m?u/NgEfM|0t0/]KIL9=),;$w]S1*b|657' );
define( 'AUTH_SALT',        'daA8MrDRPZ4o,[P9TP>-$OLyRFCO+v1[,fZ6SFpo87ebW?mj_$ h@&7<RhQ76[na' );
define( 'SECURE_AUTH_SALT', '1!Bbp,zf(WeCNeuF?UgVT^jIA<ZBdp?}~9!kze}oC/Z4`M`=fu?x2K`xt[[T3Za)' );
define( 'LOGGED_IN_SALT',   'wioqXY1jaCV{x{DIZ,z}-)o?OM C|O@FZzm~>qusYL?Gw@/87C7?f>xwXZ34%xCn' );
define( 'NONCE_SALT',       'ni^<:Z#IS-$30~UgkALaTtH&sgO@P7{TdpXA~W+l`-hSWxRG(WaX!<?{NIAm/P-S' );
/** Absolute path to the WordPress directory. */

if ( !defined('ABSPATH') )
 define('ABSPATH', dirname(__FILE__) . '/');

define('CONCATENATE_SCRIPTS', false);

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
