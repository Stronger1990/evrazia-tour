<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package evrazia-tour
 */

get_header();
?>
<h1 class="hide">Поиск и бронирование туров – Евразия Тур</h1>
<div class="container-slide">
        <div class="container-fluid">
            <div class="row">
                <div class="wrap-container-contact">
                    <div class="container-slide-contact">
                        <div class="contact-slide-title">
                            <h2>Выбрать тур</h2>
                        </div>
                        <div class="slide-menu-contact">
                           <?php echo do_shortcode('[contact-form-7 id="5" title="Подобрать тур"]'); ?>
                        </div>
                    </div>
                </div>
                <div id="main-slider" class="container-sliders">
                    <div class="main-slider-item">
                        <div class="main-slider-item-image main-slider-road-forest"></div>
                        <div class="main-slider-item-title">
                            <h2 class="slider-header text-white">Устрой себе отпуск прямо сейчас!</h2>
                             <div class="background-color-yellow"></div>
                            <p class="slider-paragraph-middle text-white">Бесплатный подбор тура</p>
                            <button class="btn-biggest button-open-paper-modal" data-toggle="modal" data-target="#myModalcall">Заказать тур</button>
                        </div>
                    </div>
                    <!--<div class="main-slider-item">
                        <div class="main-slider-item-image main-slider-road-forest"></div>
                        <div class="main-slider-item-title">
                            <h2 class="slider-header text-white">Устрой себе отпуск прямо сейчас!</h2>
                             <div class="background-color-yellow"></div>
                            <p class="slider-paragraph-middle text-white">Бесплатный подбор тура</p>
                            <button class="btn-biggest button-open-paper-modal" data-toggle="modal" data-target="#myModalcall">Заказать тур</button>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
    <!-- end contact-form -->

    <div class="container-content">

        <!-- container-module -->
        <div class="container-block-module">
            <div class="container-wrap-block-width">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="container-block-margin-title">
                                <h2>Найди свой тур прямо сейчас</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="wrap-container-block-module">
                                <script type="text/javascript" language="JavaScript" src="http://tourclient.ru/f/jsboot/34940/find_tour_form?style=default&conf=default"></script>
                                <script type="text/javascript" language="JavaScript" src="http://tourclient.ru/f/jsboot/34940/find_tour_offers?style=default&conf=default"></script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end container-module -->

        <!-- container-tile -->
        <div class="container-wrapper-tile">
            <div class="container-wrap-block-width">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="container-block-margin-title">
                                <h2>Выбрать тур</h2>
                                <span></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="wrap-container-block-tile">
                                <a href="http://evrazia-tour.ru/tury-po-evrope/">
                                <div class="container-block-tile s3-animator-hide">
                                    <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/icon-tile-1.png'?>">
                                    <h4>Туры по Европе</h4>
                                </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="wrap-container-block-tile">
                                <div class="container-block-tile s3-animator-hide">
                                    <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/icon-tile-11.png'?>">
                                    <h4>Бронирование отелей в Европе</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                             <div class="wrap-container-block-tile">
                               <a href="http://evrazia-tour.ru/kruizy/">
                                <div class="container-block-tile s3-animator-hide">
                                    <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/icon-tile-8.png'?>">
                                    <h4>Круизы</h4> 
                                </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="wrap-container-block-tile">
                               <a href="http://evrazia-tour.ru/tury-iz-yaroslavlya/">
                                <div class="container-block-tile s3-animator-hide">
                                    <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/icon-tile-10.png'?>">
                                    <h4>Туры из Ярославля</h4>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="wrap-container-block-tile">
                               <a href="http://evrazia-tour.ru/aviabilety/">
                                <div class="container-block-tile s3-animator-hide">
                                    <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/icon-tile-12.png'?>">
                                    <h4>Авиабилеты</h4> 
                                </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="wrap-container-block-tile">
                               <a href="http://evrazia-tour.ru/transfery/">
                                <div class="container-block-tile s3-animator-hide">
                                    <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/icon-tile.png'?>">
                                    <h4>Трансферы</h4>
                                </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                           <div class="wrap-container-block-tile">
                                <div class="container-block-tile s3-animator-hide">
                                    <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/icon-tile-9.png'?>">
                                    <h4>Авторские туры и Фитнес туры</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                           <div class="wrap-container-block-tile">
                                <div class="container-block-tile s3-animator-hide">
                                    <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/icon-tile-6.png'?>">
                                    <h4>Детский отдых</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end container-tile -->

        <!-- container-hot-instagram -->
        <div class="container-wrapper-instagram">
            <div class="container-fluid">
              <div class="wrapper-container-wrap-block-width">
               <div class="container-wrap-block-width">
               <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="container-block-margin-title">
                                <h2>Топовые предложения</h2>
                                <span>Подписывайтесь на наш instagram</span>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="container-block-instagram">
                            <?php echo do_shortcode('[instagram-feed num=4 cols=4 imagepadding=10 buttontext="Больше постов" followtext="Подписаться" type=hashtag]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end container-hot-instagram -->

        <!-- container-country -->
        <div class="container-wrapper-country">
            <div class="container-wrap-block-width">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="container-block-margin-title">
                                <h2 class="text-white">Страны</h2>
                                <span>Все страны мира</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-8">
                            <div class="container-slider-block-country s3-animator-hide">
                                <div id="country-slider" class="container-slider">
                                    <div class="slide-country">
                                        <div class="container-block-country">
                                            <a href="http://evrazia-tour.ru/rossiya/">
                                            <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/russia.png'?>">
                                            <span>Россия</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="slide-country">
                                        <div class="container-block-country">
                                            <a href="http://evrazia-tour.ru/andora/">
                                            <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/country-2.png'?>">
                                            <span>Андора</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="slide-country">
                                        <div class="container-block-country">
                                            <a href="http://evrazia-tour.ru/bahrejn/">
                                            <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/country-3.png'?>">
                                            <span>Бахрейн</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="slide-country">
                                        <div class="container-block-country">
                                            <a href="http://evrazia-tour.ru/belarus/">
                                            <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/country-4.png'?>">
                                            <span>Беларусь</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="slide-country">
                                        <div class="container-block-country">
                                            <a href="http://evrazia-tour.ru/dominikana/">
                                            <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/country-5.png'?>">
                                            <span>Доминикана</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="slide-country">
                                        <div class="container-block-country">
                                            <a href="http://evrazia-tour.ru/oae/">
                                            <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/country-6.png'?>">
                                            <span>ОАЭ</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="slide-country">
                                        <div class="container-block-country">
                                            <a href="http://evrazia-tour.ru/tailand/">
                                            <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/country-1.png'?>">
                                            <span>Таиланд</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end container-country -->

        <!-- container-reviews -->
        <div class="container-wrapper-reviews">
           <div id="container-request-call-parallax" class="hidden-xs"></div>
            <div class="container-wrap-block-width">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="container-block-margin-title">
                                <h2 class="text-white">Отзывы туристов</h2>
                            </div>
                        </div>
                    </div>
                    <div class="container-wrapper-block-reviews">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div id="reviews-slider" class="container-slider">
                                    <div class="slide-reviews s3-animator-hide">
                                        <div class="wrap-container-block-reviews">
                                            <div class="container-block-reviews">
                                                <div class="container-block-reviews-content">
                                                    <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/foto-reviews-3.png'?>">
                                                    <p>Здравствуйте! Мы прибыли, все замечательно! Понравилось! Немного поборолись в отеле за хороший номер, а отель замечательный, кормят хорошо, живая музыка каждый вечер, черепахи приплывают. Хорошие люди, приветливые и гостеприимные. Есть что посмотреть, только добираться долго, от нас не менее трех часов все места экскурсий. Были на "Китах" не пожалели - видели - потрясающее зрелище. Все экскурсии очень не плохие, уена недорого! Место отдыха:  Шри ланка!</p>
                                                </div>
                                                <div class="container-block-reviews-name">
                                                    <span>Ирина</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-reviews s3-animator-hide">
                                        <div class="wrap-container-block-reviews">
                                            <div class="container-block-reviews">
                                                <div class="container-block-reviews-content">
                                                    <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/foto-reviews-2.png'?>">
                                                    <p>Добрый день! Нам все очень понравилось, начиная от перевозчика и заканчивая самим отдыхом. Заранее подготовились к требованиям а/к (ручная кладь, электронная регистрация и пр.), поэтому проблема с "Победой" не было. Самолеты новые, персонал вежливый, пилоты очень крутые, влет и посадка были идеальными, рейсы не задержались. Большое спасибо за организацию нашего отдыха! Место отдыха: Стамбул!</p>
                                                </div>
                                                <div class="container-block-reviews-name">
                                                    <span>Ольга</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-reviews s3-animator-hide">
                                        <div class="wrap-container-block-reviews">
                                            <div class="container-block-reviews">
                                                <div class="container-block-reviews-content">
                                                    <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/foto-reviews-1.png'?>">
                                                    <p>Отдых супер! Нам все очень понравилось: природа и погода, и люди, еда... и гид замечательный был))) Мы очень довольны! Спасибо Вам большое! Место отдыха: Армения!</p>
                                                </div>
                                                <div class="container-block-reviews-name">
                                                    <span>Марина</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-reviews s3-animator-hide">
                                        <div class="wrap-container-block-reviews">
                                            <div class="container-block-reviews">
                                                <div class="container-block-reviews-content">
                                                    <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/foto-reviews-2.png'?>">
                                                    <p>Девочки, спасибо вам. Добрался отлично, отель классный! Отдохнули очень круто. Все понравилось. Дорога к морю проходит через красивый парк. Отель стоит за городом, поэтому там тихо и спокойно. Крытый бассейн, хороший тренажерный зал, спа. В отеле основной контингент это европейцы (немцы, французы) были и русские ребята. Мы все быстро познакомились. Каждый вечер развлекательные программы.</p>
                                                </div>
                                                <div class="container-block-reviews-name">
                                                    <span>Денис</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-reviews s3-animator-hide">
                                        <div class="wrap-container-block-reviews">
                                            <div class="container-block-reviews">
                                                <div class="container-block-reviews-content">
                                                    <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/foto-reviews-2.png'?>">
                                                    <p>Добрый день! Нам все очень понравилось, начиная от перевозчика и заканчивая самим отдыхом. Заранее подготовились к требованиям а/к (ручная кладь, электронная регистрация и пр.), поэтому проблема с "Победой" не было. Самолеты новые, персонал вежливый, пилоты очень крутые, влет и посадка были идеальными, рейсы не задержались. Большое спасибо за организацию нашего отдыха!</p>
                                                </div>
                                                <div class="container-block-reviews-name">
                                                    <span>Ольга</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-reviews s3-animator-hide">
                                        <div class="wrap-container-block-reviews">
                                            <div class="container-block-reviews">
                                                <div class="container-block-reviews-content">
                                                    <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/foto-reviews-2.png'?>">
                                                    <p>Добрый день! Нам все очень понравилось, начиная от перевозчика и заканчивая самим отдыхом. Заранее подготовились к требованиям а/к (ручная кладь, электронная регистрация и пр.), поэтому проблема с "Победой" не было. Самолеты новые, персонал вежливый, пилоты очень крутые, влет и посадка были идеальными, рейсы не задержались. Большое спасибо за организацию нашего отдыха!</p>
                                                </div>
                                                <div class="container-block-reviews-name">
                                                    <span>Ольга</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end container-reviews -->

        <!-- container-advantages -->
        <div class="container-wrapper-advantages">
            <div class="container-wrap-block-width">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-7">
                            <div class="container-block-advantages-list">
                                <h2>Почему стоит выбрать нас?</h2>
                                <ul class="advantages-list">
                                    <li class="block-advantages-list s3-animator-hide">
                                        <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/icon-advantages-3.png'?>">
                                        <div class="block-advantages-list-text">
                                            <h4>Мы всегда на связи</h4>
                                            <p>Наш мобильный телефон всегда включен,  и Вы можете круглосуточно дозвониться до нас. В любое время суток мы решаем редко случающиеся проблемы в кратчайшие сроки.</p>
                                        </div>
                                    </li>
                                    <li class="block-advantages-list s3-animator-hide">
                                        <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/icon-advantages-2.png'?>">
                                        <div class="block-advantages-list-text">
                                            <h4>У нас есть что Вам предложить</h4>
                                            <p>Мы всегда подбираем туры по выгодным ценам, будь то «горящие путевки» или базовые турпакеты. Мы всегда предложим Вам исключительные скидки по Раннему Бронированию. В отличие от других агентств мы не делаем наценку на стоимость туров туроператоров.</p>
                                        </div>
                                    </li>
                                    <li class="block-advantages-list s3-animator-hide">
                                        <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/icon-advantages-1.png'?>">
                                        <div class="block-advantages-list-text">
                                            <h4>Наши менеджеры лучшие в своем деле</h4>
                                            <p>Мы уделяем большое внимание подготовке кадров. Стаж работы менеджеров минимум от 2х до 18ти лет.</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-5">
                            <div class="container-block-advantages-img">
                                <img class="" alt="..." src="<?php echo get_template_directory_uri() . '/assets/img/unnamed.png'?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end container-advantages -->
    </div>	
<?php
get_footer();
