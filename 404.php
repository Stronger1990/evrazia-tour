<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package evrazia-tour
 */

get_header();
?>

	<div class="container-content-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="page-content-block-wrapper">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                           <div class="error-404">
                            <h1>Такой страницы нет! Перейти на <a href="http://evrazia-tour.ru">Главную</a></h1> 
                            <img class="" alt="ошибка" src="<?php echo get_template_directory_uri() . '/assets/img/error-404.jpg'?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
