<?php
/*
Template Name: Страница с выводом стран
*/
get_header();
?>
<div class="container-content-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="page-content-block-wrapper">
                    
                    <?php query_posts('cat=3'); ?>

                        <?php if (have_posts()) : ?>

                            <?php while (have_posts()) : the_post(); ?>
                             <div class="wrap-post-content">
                        <div class="container">
                            <div class="row">
                              
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div class="img-responsive">
                                        <?php the_post_thumbnail(); ?>
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                
                                    <div class="wrap-post-title">
                                        <a href="<? the_permalink(); ?>" class="">
                                            <h2>
                                                <?php the_title(); ?>
                                            </h2>
                                        </a>
                                    </div>
                                    <div class="wrap-post-text">
                                        <div style="font-weight: 300;">
                                            <?php the_excerpt(); ?>
                                        </div>
                                        <strong class="responsive-low-text">
                                            <?php echo(get_post_meta($post->ID, 'name', true)); ?>
                                        </strong>
                                        <a href="<? the_permalink(); ?>" class="">
                                            Читать далее...
                                        </a>
                                    </div>
                                </div>
                              
                                
                            </div>
                        </div>
                    </div>
                        <?php endwhile; ?>

                <?php else : ?>

    <h2>Записей нет</h2>

<?php endif; ?>

<?php wp_reset_query(); ?>

                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
