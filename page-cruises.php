<?php
/*
Template Name: Страница круизы
*/
get_header();
?>
<div class="container-content-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
               <div class="title-cruises">
                   <?php the_title('<h1>','</h1>'); ?>
               </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sx-12 col-sm-6 col-md-6">
                <div class="img-cruises">
                    <a href="http://evrazia-tour.ru/kruizy/rechnye-kruizy/"><img src="http://evrazia-tour.ru/wp-content/uploads/2019/05/icort9.jpg" alt="" class="wp-image-149"/></a>
                </div>
            </div>
            <div class="col-sx-12 col-sm-6 col-md-6">
                <div class="img-cruises">
                    <a href="http://evrazia-tour.ru/kruizy/morskie-kruizy/"><img src="http://evrazia-tour.ru/wp-content/uploads/2019/05/icort8.jpg" alt="" class="wp-image-149"/></a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
