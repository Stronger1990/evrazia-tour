<?php
/*
Template Name: Страница контакты
*/
get_header();
?>
<div class="container-content-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h3>Единый бесплатный телефон 8 800 222 46 56</h3>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="item-tabs-contact-wrapper">
                    <div class="item-tab-delivery-wrap">
                        <div id="button-delivery" class="item-tab-title buttun-delivery-active">
                            <h5>г. Ярославль</h5>
                        </div>
                    </div>
                    <div class="item-tab-delivery-wrap">
                        <div id="button-payment" class="item-tab-title">
                            <h5>Нижегородская область</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="content-delivery">
            <?php
                get_template_part( "content", "delivery" );
            ?>
        </div>

        <div id="content-payment" class="hide">
            <?php
                get_template_part( 'content', 'payment' );
            ?>
        </div>
    </div>
</div>
<?php
get_footer();
